<?php
//include callback cart
require_once './cart.inc';
if (isset($_GET["id"])) {
	switch ($_GET["id"]) {
		case 1:
			$product = "APPLE";
			break;
		case 2:
			$product = "ASUS";
			break;
		case 3:
			$product = "DELL";
			break;
		case 4:
			$product = "LENOVO";
			break;								
		default:
			$product = "";
			break;
	}
}
$page_title = "<h4><span style='font-size: 25px; color: #0B8109;'>Sản Phẩm $product</span></h4>";
$base_filename = basename(__FILE__, '.php');
$page_body_file = "$base_filename/$base_filename.body.tpl";

include 'views/_layout.php';