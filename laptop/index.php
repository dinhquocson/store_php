<?php
require_once './cart.inc';
$page_title = "<h4><span style='font-size: 25px; color: #1A4623;'>Trang Chủ</span></h4>";

$base_filename = basename(__FILE__, '.php');
$page_body_file = "$base_filename/$base_filename.body.tpl";

include 'views/_layout.php';
