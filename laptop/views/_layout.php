<?php
// capcha
// session_start();
require_once './vendor/autoload.php';
use Gregwar\Captcha\CaptchaBuilder;
require_once './lib/db.php';
require_once './customer/login.php';
require_once './customer/signup.php';
// unset($_SESSION["cart"]);
require_once './cart.inc';
// unset($_SESSION["dang_nhap_chua"]);
// 	unset($_SESSION["current_user"]);
// 		// xoá cookie auth_user_id
// 	setcookie("auth_user_id", "", time() - 3600);
// $sql = "INSERT INTO categories (CatID,CatName)
// VALUES (5, 'Doe')";
// $rs = load($sql);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Online shop</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		
		<link rel="stylesheet" type="text/css" href="/laptop/public/dropdown.css">
		<link rel="stylesheet" type="text/css" href="/laptop/public/style.css">
		<link rel="stylesheet" type="text/css" href="/laptop/public/login_signup.css">
		<script src="/laptop/public/jquerya.js" type="text/javascript"></script>

		<script rel="stylesheet" type="text/javascript" src="/laptop/public/a.js" ></script>
		<link rel="stylesheet" type="text/css" href="/laptop/public/test.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css">
	</head>
<style>
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}
</style>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="btn btn-info btn-lg" style="margin-top: 7px;position :relative;top: 10px;">
						<span class="glyphicon glyphicon-home"></span> Home
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i style="font-size: 40px;position :relative;left: 43px;" class="fa fa-laptop" aria-hidden="true"></i>  <span style="position :relative; top : 20px; right: 10px">LAPTOP</span> </a>
							<div class="dropdown-content">
								<?php
								$sql = "select * from Categories";
								$rs = load($sql);
								while ($row = $rs->fetch_assoc()):
								?>
								<a href="productsByCat.php?id=<?=$row["CatID"]?>&page=1" class="list-group-item"><?=$row["CatName"]?></a>
								<?php
								endwhile;
								?>
							</div>
						</li>
					</ul>
					
					<form action="index.php" method="GET">
						<div class="col-xs-6 col-md-4" style="position :relative;top: 10px;width: 700px;">
							
							<div class="input-group">
								<input style="" name="txtSearch"  type="text" class="form-control" placeholder="Nhập tên máy tính . . . cần tìm" id="txtSearch"/>
								<div class="input-group-btn" >
									<button class="btn btn-danger " type="submit">
									<span class="glyphicon glyphicon-search"></span>
									</button>
								</div>
								
							</div>
						</div>
					</form>
					
					<ul class="nav navbar-nav navbar-right">
						<?php
						if (!isset($_SESSION["dang_nhap_chua"])) {
						$_SESSION["dang_nhap_chua"] = 0;
						}
						if ($_SESSION["dang_nhap_chua"] == 0) {
						?>
						<li>
							<a href="viewcart.php"><i class="fa fa-2x fa-shopping-cart" ></i></a>
						</li>
						<li>
							<lavel class="label label-danger" class="badge badge-warning"><?= get_total_items() ?></lavel>
						</li>
						<li>
							<a href="#" onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
						</li>
						<li>
							<div id="id01" class="modal" >
								<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">×</span>
								<form id="formSignup" class="modal-content animate" action="" method="POST" role="form" >
									<div class="container" >
										<label><b>Full Name</b></label>
										<input type="text" placeholder="Enter Full Name" name="txt-FName" required >
										<label><b>Email</b></label><br>
										<input type="email" placeholder="Enter Email" name="txtEmail" required style ="height: 35px; width: 700px;";><br><br>
										<label><b>Date</b></label><br>
										<input type="Date" placeholder="Enter Date" name="txtDate" required style ="height: 35px; width: 700px;";><br><br>
										<label><b>UserName</b></label>
										<input type="text" placeholder="Enter UserName" name="UserName" required id="UserName">
										<label><b>Password</b></label>
										<input type="Password" placeholder="Enter Password" name="txtPassword" required id="txtPassword">
										<label><b>Repeat Password</b></label>
										<input type="password" placeholder="Repeat Password" name="psw-repeat" required id="psw-repeat">
										<div class="form-group">
											<?php
												$builder = new CaptchaBuilder;
												$builder->build();
												$_SESSION["captcha"] = $builder->getPhrase();
											?>
											<img src="<?= $builder->inline() ?>" alt="captcha" />
										</div>
										<div class="form-group">
											<label for="txtUserInput">Captcha</label>
											<input type="text" class="form-control" id="txtCaptcha" name="txtCaptcha" required>
										</div>
										<div class="clearfix">
											<button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
											<button type="submit" class="signupbtn" name="btnSignup" id="submit_value" >Sign Up</button>
										</div>
									</div>
								</form>
							</div>
						</li>
						<li>
							<a href="#" onclick="document.getElementById('id02').style.display='block'" style="width:auto;"><span class="glyphicon glyphicon-log-in""></span> Login</a>
						</li>
						<li>
							<div id="id02" class="modal">
								<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">×</span>
								<form method="post" class="modal-content animate" action="" >
									<div class="container" >
										<label><b>Username</b></label>
										<input id="txtUserName" type="text" placeholder="Enter Username" name="txtUserName" required>
										<label><b>Password</b></label>
										<input id="txtPassword" type="password" placeholder="Enter Password" name="txtPassword" required>
										<input type="checkbox" name="cbRemember"> Remember me
										<div class="container" style="background-color:#f1f1f1">
											<button type="submit" class="signupbtn" name="btnLogin">Login</button>
											<button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
											<span class="psw">Forgot <a href="#">password?</a></span>
										</div>
									</div>
								</form>
							</div>
						</li>
						<?php
						} else {
						?>
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="viewcart.php"><i class="fa fa-2x fa-shopping-cart"></i></a>
							</li>
							<li>
								<lavel class="label label-danger" class="badge badge-warning"><?= get_total_items() ?></lavel>
							</li>

							<li class="dropdown">

								<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i style="font-size: 25px " class="glyphicon glyphicon-user" aria-hidden="true"></i>XinChào <b> <?=$_SESSION["current_user"]->f_Name?></b </a>
								</a>
								
								<div class="dropdown-content">
									<a href="profile.php" class="list-group-item">Thông Tin Cá Nhân</a>
									<?php 
										$Admin = $_SESSION["current_user"]->f_Permission;			
										if($Admin === "1"){
											echo "<a href='addProduct.php' class='list-group-item'>Thêm Sản Phẩm Mới</a>";

											echo "<a href='status.php' class='list-group-item'>Cập Nhật Trạng Thái Đơn Hàng</a>";
											echo "<a href='quanlysanpham.php' class='list-group-item'>Quản Lý Sản Phẩm</a>";

										}else
											echo "<a href='productOrderUser.php' class='list-group-item'>Đơn Hàng Đã Đặt</a>";												
									?>									
									<a href="/laptop/customer/logout" class="list-group-item">Thoát</a>
								</div>
							</li>
						</ul>
						<?php
						}
						?>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<div class="position" style="position: fixed; width: 210px;top: 15;">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">
								Giá</h3>
								<span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
							</div>
							<div class="panel-body">
								<ul class="list-group">
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox1" type="checkbox">
											<label for="checkbox1">
												0-10tr
											</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox2" type="checkbox">
											<label for="checkbox2">
												10-20tr
											</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox3" type="checkbox">
											<label for="checkbox3">
												20tr-30tr
											</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox4" type="checkbox">
											<label for="checkbox4">
												trên 30tr
											</label>
										</div>
									</li>
								</ul>
								
								
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">
								Nhà sản xuất</h3>
								<span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
							</div>
							<div class="panel-body">
								<ul class="list-group">
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox5" type="checkbox">
											<label for="checkbox5">
												Apple
											</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox6" type="checkbox">
											<label for="checkbox6">
												Asus
											</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox7" type="checkbox">
											<label for="checkbox7">
												Dell
											</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="checkbox checkbox-info">
											<input id="checkbox8" type="checkbox">
											<label for="checkbox8">
												Lenovo
											</label>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
					<div class="panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?=$page_title?></h3>
						</div>
						<div class="panel-body">
							<?php include_once $page_body_file;?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="assets/jquery-3.1.1.min.js"></script>
		<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<!-- script search Giá and Nhà sản xuất -->
		<script type="text/javascript">
					$('#checkbox1').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=10000000');
						}
					});
					$('#checkbox2').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=20000000');
						}
					});
					$('#checkbox3').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=30000000');
						}
					});
					$('#checkbox4').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=30000069');
						}
					});
					$('#checkbox5').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=1&page=1');
						}
										});
					$('#checkbox6').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=2&page=1');
						}
							});
					$('#checkbox7').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=3&page=1');
						}
					});
					$('#checkbox8').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=4&page=1');
						}
																							});
		// <!-- script check user signup -->
		$('#formSignup').on('submit', function (e) {
		e.preventDefault();
		var form = this;
		var username = $('#UserName').val();
		var password = $('#txtPassword').val();
		var pw2 = $('#psw-repeat').val();
		var captcha = $('#txtCaptcha').val();
		if (username.length < 4  || /^[a-zA-Z0-9- ]*$/.test(username) == false ) {
		alert('username không hợp lệ');
		return;
		}
		if(password.length < 4 || /^[a-zA-Z0-9- ]*$/.test(password) == false ){
		alert('password không hợp lệ');
		return;
		}
		if(password != pw2){
		alert('password nhập lại không khớp');
		return;
		}
		alert("<?=$_SESSION["captcha"]?>");
		
		if(captcha != "<?=$_SESSION["captcha"]?>"){
		alert('chưa đúng capcha');
		return;
		}
		var url = 'api/user_check.php?user=' + username;
		$.getJSON(url, function (data) {
		if (data === 1) {
		alert('user đã tồn tại');
		return;
		} else {
		document.getElementById("formSignup").submit();
		}
		});
		});
		</script>
	</body>
</html>