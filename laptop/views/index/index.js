$(document).ready(function() {
	size_li = $("#myList li").length;
	x = 6;
	if (x > size_li) {

		document.getElementById("loadMore").style.visibility = "hidden";

	}
	$('#myList li:lt(' + x + ')').show();
	$('#loadMore').click(function() {
		x = (x + 3 <= size_li) ? x + 3 : size_li;
		if (x >= size_li) {
			document.getElementById("loadMore").style.visibility = "hidden";
		}
		$('#myList li:lt(' + x + ')').show();
	});

});