<?php

// if (!isset($_GET["id"]) || !isset($_SESSION["current_user"])) {
//     header("Location: index.php");
// }

?>



<!-- edit user -->
<?php

if (isset($_POST["btnEdit"])) {

$ID=$_SESSION["current_user"]->f_ID;
$fullName = $_POST["txt-FullName"];
$email = $_POST["txtEmail"];
$date = $_POST["txtDate"];
$sqlEdit = "UPDATE users SET f_Name = '$fullName' ,f_Email = '$email' ,f_DOB = '$date' WHERE f_ID = $ID";
$rsEdit = write($sqlEdit);
echo "<div class='panel panel-success' style='margin: 1em;'>
    <div class='panel-heading'>
        <h3 class='panel-title'><label for='new_password' class='control-label panel-title'>Cập Nhật Thông Tin Thành Công</label></h3>
    </div>
</div>";
}
?>
<!-- change password -->
<?php 
if (isset($_POST["btnChangePW"])) {
$ID=$_SESSION["current_user"]->f_ID;
$password = $_POST["newPassword"];
$cf_password = $_POST["password_confirmation"];
if($password === $cf_password){
    // mã hoá trước khi update
    $enc_password = md5($password);
    $sqlChangePW = "UPDATE users SET f_Password = '$enc_password' WHERE f_ID = $ID";
    $rsPW = write($sqlChangePW);
    echo "<div class='panel panel-success' style='margin: 1em;'>
    <div class='panel-heading'>
        <h3 class='panel-title'><label for='new_password' class='control-label panel-title'>Thay Đổi Mật Khẩu Thành Công</label></h3>
    </div>
</div>";
}
else{
        echo "<div class='panel panel-success' style='margin: 1em;'>
    <div class='panel-heading'>
        <h3 class='panel-title'><label for='new_password' class='control-label panel-title'>Confirm password Không Khớp với New PassWord</label></h3>
    </div>
</div>";
}

}
 ?>

<div class="row">
    
    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked admin-menu" >
            <li class="active"><a href="" data-target-id="profile"><i class="glyphicon glyphicon-userb"></i> Profile</a></li>
            <li><a href="" data-target-id="change-password"><i class="glyphicon glyphicon-lock"></i> Change Password</a></li>
            <li><a href="" data-target-id="settings"><i class="glyphicon glyphicon-cog"></i> Settings</a></li>
            <li><a href="" data-target-id="logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
        </ul>
    </div>
    <?php 
    $ID=$_SESSION["current_user"]->f_ID;
    $sql = "select * from users where f_ID = $ID";
    $rs = load($sql);
    if ($rs->num_rows > 0)
    while ($row = $rs->fetch_assoc()) :
    ?>

    <div class="col-md-6   admin-content" id="profile">
        <form action="profile.php" method="post">
            <div class="panel panel-success" style="margin: 1em;">
                <div class="panel-heading">
                    <h3 class="panel-title">FULL NAME </h3>
                    
                </div>
                <div class="panel-body">
                    <!-- load data -->
                    <input type="text" class="form-control" name="txt-FullName" id=""
                    value="<?=$row["f_Name"] ?>">
                </div>
            </div>
            <div class="panel panel-success" style="margin: 1em;">
                <div class="panel-heading">
                    <h3 class="panel-title">Email</h3>
                </div>
                <div class="panel-body">

                    <input type="text" class="form-control" name="txtEmail" id=""
                    value="<?=$row["f_Email"] ?>">
                </div>
            </div>
            <div class="panel panel-success" style="margin: 1em;">
                <div class="panel-heading">
                    <h3 class="panel-title">Date birth</h3>
                </div>
                <div class="panel-body">

                    <input type="date" required value="<?=$row["f_DOB"] ?>" name="txtDate">
                    
                </div>
            </div>
            <div class="panel panel-success" style="margin: 1em;">
                <div class="panel-heading">
                    <h3 class="panel-title">UserName</h3>
                </div>
                <div class="panel-body">

                    <input type="text" class="form-control" name="UserName" id=""
                    value="<?=$row["f_Username"] ?>" readonly>
                </div>
            </div>
            <div class="panel panel-success" style="margin: 1em;">
                <div class="panel-body">
                    <button type="submit" class="signupbtn" name="btnEdit">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <?php
    endwhile;
    ?>
    <div class="col-md-8   admin-content" id="settings">
        <div class="panel panel-success" style="margin: 1em;">
            <div class="panel-heading">
                <h3 class="panel-title">Notification</h3>
            </div>
            <div class="panel-body">
                <div class="label label-success">allowed</div>
            </div>
        </div>
        <div class="panel panel-success" style="margin: 1em;">
            <div class="panel-heading">
                <h3 class="panel-title">Newsletter</h3>
            </div>
            <div class="panel-body">
                <div class="badge">Monthly</div>
            </div>
        </div>
        <div class="panel panel-success" style="margin: 1em;">
            <div class="panel-heading">
                <h3 class="panel-title">Admin</h3>
            </div>
            <div class="panel-body">
                <div class="label label-success">yes</div>
            </div>
        </div>
    </div>
    <div class="col-md-8  admin-content" id="change-password">
        <!-- tại trang này xử lý nên action "" -->
        <form action="" method="post">
            
            <div class="panel panel-success" style="margin: 1em;">
                <div class="panel-heading">
                    <h3 class="panel-title"><label for="new_password" class="control-label panel-title">New Password</label></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-lg-5">
                            <input type="password" class="form-control" name="newPassword" id="new_password" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-danger" style="margin: 1em;">
                <div class="panel-heading">
                    <h3 class="panel-title"><label for="confirm_password" class="control-label panel-title">Confirm password</label></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-lg-5">
                            <input type="password" class="form-control" name="password_confirmation" id="confirm_password" >
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-success" style="margin: 1em;">
                <div class="panel-body">
                    <button type="submit" class="signupbtn" name="btnChangePW">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <div class=col-md-8  admin-content" id="settings"></div>
    <div class="col-md-8   admin-content" id="logout">
        <div class="panel panel-danger" style="margin: 1em;">
            <div class="panel-heading">
                <h3 class="panel-title">Confirm Logout</h3>
            </div>
            <div class="panel-body">
                Do you really want to logout ?
                <a  href="#" class="label label-danger"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <span >   YES  </span>
                </a>
                <a href="/account" class="label label-success"> <span >  NO   </span></a>
            </div>
            <form id="logout-form" action="#" method="POST" style="display: none;">
            </form>
        </div>
    </div>
</div>