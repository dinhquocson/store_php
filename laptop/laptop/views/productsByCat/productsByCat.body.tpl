<?php
if (!isset($_GET["id"])) {
	header('Location: index.php');
}
?>
<?php
$id = $_GET["id"];
$limit = 3;
$current_page = 1;
if (isset($_GET["page"])) {
	$current_page = $_GET["page"];
}
$next_page = $current_page + 1;
$prev_page = $current_page - 1;
$c_sql = "select count(*) as num_rows from products where CatID = $id";
$c_rs = load($c_sql);
$c_row = $c_rs->fetch_assoc();
$num_rows = $c_row["num_rows"];
$num_pages = ceil($num_rows / $limit);
	if ($current_page < 1 || $current_page > $num_pages	) {
	$current_page = 1;
	
	if (headers_sent()) {
		die("Khong tim thay san pham:<a href='http://localhost:8080/laptop/productsByCat.php?id=1&page=1'>nhấn để quay về </a>");
	}
}
// $offset = 0;
$offset = ($current_page - 1) * $limit;
$sql = "select * from products where CatID = $id limit $offset, $limit ";
$rs = load($sql);
?>
<div class="row">
	<?php
	if ($rs->num_rows > 0):
	while ($row = $rs->fetch_assoc()):
	?>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail">
			<img src="imgs/sp/<?=$row["ProID"]?>/main_thumbs.jpg" alt="...">
			<div class="caption">
				<h4 ><?=$row["ProName"]?></h4>
				<h4 style="color: #D00230"><?=number_format($row["Price"])?> Đ</h4>
				<p style="height: 40px"><?=$row["TinyDes"]?></p>
				<br>
				<p>
					<a href="productDetails.php?ProID=<?=$row["ProID"]?>" class="btn btn-primary" role="button">Chi tiết</a>
					<a href="#" class="btn btn-danger" role="button">
						<span class="glyphicon glyphicon-shopping-cart"></span>
						Đặt mua
					</a>
				</p>
			</div>
		</div>
	</div>
	<?php
	endwhile;
	endif;
	?>
	
	<div colspan="4" class="text-center">
		<?php if ($prev_page > 0) : ?>
		<a class="btn btn-primary" href="productsByCat?id=<?=$id?>&page=<?= $prev_page ?>" role="button">
			<span class="glyphicon glyphicon-arrow-left"></span>
			Prev
		</a>
		<?php endif; ?>
		<?php if ($next_page <= $num_pages) : ?>
		<a class="btn btn-primary" href="productsByCat?id=<?=$id?>&page=<?= $next_page ?>" role="button">
			<span class="glyphicon glyphicon-arrow-right"></span>
			Next
		</a>
		<?php endif; ?>
	</div>
	
	
</div>