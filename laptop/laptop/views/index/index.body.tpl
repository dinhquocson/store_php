<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/laptop/views/index/a.css">
<script src="/laptop/views/index/index.js" type="text/javascript"></script>
<div class="jumbotron">
			
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<a href="productDetails.php?ProID=14"><img src="imgs/sp/14/2.jpg" alt="Chicago" style="width:100%;" >	</a>		
					</div>
					<div class="item">
						<a href="productDetails.php?ProID=19"><img src="imgs/sp/19/3.jpg" alt="Chicago" style="width:100%;" >	</a>
						
					</div>
					
					<div class="item">
						
						<a href="productDetails.php?ProID=37"><img src="imgs/sp/37/3.gif" alt="Chicago" style="width:100%;" >	</a>
					</div>
				</div>
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
<div class="row">

	<?php

	if (isset ($_GET["txtSearch"])) {

	?>
	<div class="row">
		<ul class="list-unstyled" id="myList">
			
			<?php
				$txtSearch = $_GET["txtSearch"];
					
					if($txtSearch > 30000000){
						$sql = "select * from products where Price >= $txtSearch ";
					}
					switch ($txtSearch) {
						case 10000000:
							$sql = "select * from products where Price <= $txtSearch";
							break;
						case 20000000:
							$sql = "select * from products where Price > 10000000 and Price <= $txtSearch";
							break;
						case 30000000:
							$sql = "select * from products where Price > 20000000 and Price <= $txtSearch";
							break;													
						default:
							$sql = "select * from products where ProName LIKE '%$txtSearch%'";
							break;
					}			
					
					$rs = load($sql);
					if ($rs->num_rows > 0) :
						while ($row = $rs->fetch_assoc()) :
			
			?>
			<li>
				<div class="col-sm-6 col-md-4">
					
					
					<div class="thumbnail" >
						<img src="imgs/sp/<?= $row["ProID"] ?>/main_thumbs.jpg" alt="...">
						<div class="caption">
							<h5><?= $row["ProName"] ?></h5>
							<h4><?= $row["Price"] ?></h4>
							<p style="height: 40px"><?= $row["TinyDes"] ?></p>
							<br>
							<p>
								<a href="productDetails.php?ProID=<?=$row["ProID"]?>" class="btn btn-primary" role="button">Chi tiết</a>
								<a href="#" class="btn btn-danger" role="button">
									<span class="glyphicon glyphicon-shopping-cart"></span>
									Đặt mua
								</a>
							</p>
						</div>
					</div>
					
					
					
				</div>
			</li>
			
			<?php
					endwhile;
			?>
		</ul>
		<?php
		else :
		?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			Không có sản phẩm thoả điều kiện.
			<center >
			<p class="fs-senull-l1"><img src="img_noSearch/no.png" alt=""></p>
			<p class="fs-senull-l2">Rất tiếc chúng tôi không tìm thấy kết quả của <strong><?=$txtSearch?></strong></p>
			<div class="fs-senullbob">
				<h4>Để tìm được kết quả chính xác hơn, xin vui lòng:</h4>
				<ul >
					<li style="display: table; margin: 0px auto 0px auto;">Kiểm tra lỗi chính tả của từ khóa đã nhập</li>
					<li style="display: table; margin: 0px auto 0px auto;">Thử lại bằng từ khóa khác</li>
					<li style="display: table; margin: 0px auto 0px auto;">Thử lại bằng những từ khóa tổng quát hơn</li>
					<li style="display: table; margin: 0px auto 0px auto;">Thử lại bằng những từ khóa ngắn gọn hơn</li>
				</ul>
			</div>
			</center>
			
		</div>
		<?php
			endif;
		?>	
		<div colspan="4" class="text-center">
		
		<a  id="loadMore" class="btn btn-primary" href="productsByCat.php?id=<?=$id?>&page=<?= $prev_page ?>" role="button">
			<span class="glyphicon glyphicon-triangle-bottom"> </span>
			 	Xem Thêm
		</a>
		
	</div>
		
	</div>
	<?php
	}

	else{
	?>
	<div class="row">
		<div class="panel-heading">
			<div class="panel-footer">
				<h2>Sản Phẩm Được Xem Nhiều Nhất</h2>
			</div>
		</div>
		<?php
			//$id = $_GET["id"];
			$sql = "select * FROM products
					ORDER BY LuotXem DESC limit 10";
			$rs = load($sql);
			
			if ($rs->num_rows > 0) :
				while ($row = $rs->fetch_assoc()) :
		?>
		
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				
				<img src="imgs/sp/<?= $row["ProID"] ?>/main_thumbs.jpg" alt="...">
				<div class="caption">
					<h5><?= $row["ProName"] ?></h5>
					<h4><?= $row["Price"] ?></h4>
					<p style="height: 40px"><?= $row["TinyDes"] ?></p>
					<br>
					<p>
						<a href="productDetails.php?ProID=<?=$row["ProID"]?>" class="btn btn-primary" role="button">Chi tiết</a>
						<a href="#" class="btn btn-danger" role="button">
							<span class="glyphicon glyphicon-shopping-cart"></span>
							Đặt mua
						</a>
					</p>
				</div>
			</div>
		</div>
		<?php
				endwhile;
			else :
		?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			Không có sản phẩm thoả điều kiện.
		</div>
		<?php
			endif;
		?>
	</div>
	<div class="row">
		<div class="panel-heading">
			<div class="panel-footer">
				<h2>Sản Phẩm Mới Nhất</h2>
			</div>
		</div>
		<?php
			//$id = $_GET["id"];
			$sql = "select * FROM products
					ORDER BY NgayTiepNhan DESC limit 10";
			$rs = load($sql);
			
			if ($rs->num_rows > 0) :
				while ($row = $rs->fetch_assoc()) :
		?>
		
		<div class="col-sm-6 col-md-4">
			
			<div class="thumbnail">
				<img src="imgs/sp/<?= $row["ProID"] ?>/main_thumbs.jpg" alt="...">
				<div class="caption">
					<h5><?= $row["ProName"] ?></h5>
					<h4><?= $row["Price"] ?></h4>
					<p style="height: 40px"><?= $row["TinyDes"] ?></p>
					<br>
					<p>
						<a href="productDetails.php?ProID=<?=$row["ProID"]?>" class="btn btn-primary" role="button">Chi tiết</a>
						<a href="#" class="btn btn-danger" role="button">
							<span class="glyphicon glyphicon-shopping-cart"></span>
							Đặt mua
						</a>
					</p>
				</div>
			</div>
		</div>
		<?php
				endwhile;
			else :
		?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			Không có sản phẩm thoả điều kiện.
		</div>
		<?php
			endif;
		?>
	</div>
	<!----------------------------------------------- -->
	<div class="row">
		<div class="panel-heading">
			<div class="panel-footer">
				<h2>Bán Chạy Nhất</h2>
			</div>
		</div>
		<?php
			//$id = $_GET["id"];
			$sql = "select * FROM products
					ORDER BY SoLuongBan DESC limit 10";
			$rs = load($sql);
			
			if ($rs->num_rows > 0) :
				while ($row = $rs->fetch_assoc()) :
		?>
		
		<div class="col-sm-6 col-md-4">
			
			<div class="thumbnail">
				<img src="imgs/sp/<?= $row["ProID"] ?>/main_thumbs.jpg" alt="...">
				<div class="caption">
					<h5><?= $row["ProName"] ?></h5>
					<h4><?= $row["Price"] ?></h4>
					<p style="height: 40px"><?= $row["TinyDes"] ?></p>
					<br>
					<p>
						<a href="productDetails.php?ProID=<?=$row["ProID"]?>" class="btn btn-primary" role="button">Chi tiết</a>
						<a href="#" class="btn btn-danger" role="button">
							<span class="glyphicon glyphicon-shopping-cart"></span>
							Đặt mua
						</a>
					</p>
				</div>
			</div>
		</div>
		<?php
				endwhile;
			else :
		?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			Không có sản phẩm thoả điều kiện.
		</div>
		<?php
			endif;
		?>
	</div>
	<?php
	}
	?>
</div>