<?php
require_once './lib/db.php';
require_once './customer/login.php';
require_once './customer/signup.php';
$sql = "INSERT INTO categories (CatID,CatName)
VALUES (5, 'Doe')";
$rs = load($sql);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Online shop</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/laptop/public/dropdown.css">
		<link rel="stylesheet" type="text/css" href="/laptop/public/style.css">
		<script src="/laptop/public/jquerya.js" type="text/javascript"></script>
		<script rel="stylesheet" type="text/javascript" src="/laptop/public/a.js" ></script>
		<link rel="stylesheet" type="text/css" href="/laptop/public/test.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
		
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a  class="navbar-brand" href="index.php">
						<span><img src="img_noSearch/shop.png" alt="" style="width: 80px;height: 40px;"></span>
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i style="font-size: 20px;" class="fa fa-laptop" aria-hidden="true"></i>  LAPTOP </a>
							<div class="dropdown-content">
								<?php
								$sql = "select * from Categories";
								$rs = load($sql);
								while ($row = $rs->fetch_assoc()):
								?>
								<a href="productsByCat.php?id=<?=$row["CatID"]?>&page=1" class="list-group-item"><?=$row["CatName"]?></a>
								<?php
								endwhile;
								?>
							</div>
						</li>
						<li class="active"><a href="#">Home</a></li>
						<li class="active"><a href="#">Home</a></li>
					</ul>
					<form action="index.php" method="GET">
						<div class="col-xs-6 col-md-4">
							<div class="input-group">
								<input name="txtSearch"  type="text" class="form-control" placeholder="Nhập tên máy tính . . . cần tìm" id="txtSearch"/>
								<div class="input-group-btn" >
									<button class="btn btn-danger " type="submit">
									<span class="glyphicon glyphicon-search"></span>
									</button>
								</div>
							</div>
						</div>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<?php
						if (!isset($_SESSION["dang_nhap_chua"])) {
						$_SESSION["dang_nhap_chua"] = 0;
						}
						if ($_SESSION["dang_nhap_chua"] == 0) {
						?>
						<li>
							<a href="viewcart.php"><i class="fa fa-shopping-cart" ></i></a>
						</li>
						<li>
							<lavel class="label label-danger" class="badge badge-warning">4</lavel>
						</li>
						<li>
							<a href="#" onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
						</li>
						<li>
							<div id="id01" class="modal">
								<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">×</span>
								<form class="modal-content animate" action="" method="POST">
									<div class="container">
										<label><b>UserName</b></label>
										<input type="text" placeholder="Enter UserName" name="UserName" required>
										<label><b>Full Name</b></label>
										<input type="text" placeholder="Enter Full Name" name="txt-FName" required>
										<label><b>Email</b></label>
										<input type="text" placeholder="Enter Email" name="txtEmail" required>
										<label><b>Date</b></label>
										<input type="Date" placeholder="Enter Date" name="txtDate" required>
										<label><b>Password</b></label>
										<input type="Password" placeholder="Enter Password" name="txtPassword" required>
										<label><b>Repeat Password</b></label>
										<input type="password" placeholder="Repeat Password" name="psw-repeat" required>
										<div class="clearfix">
											<button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
											<button type="submit" class="signupbtn" name="btnSignup">Sign Up</button>
										</div>
									</div>
								</form>
							</div>
						</li>
						<li>
							<a href="#" onclick="document.getElementById('id02').style.display='block'" style="width:auto;"><span class="glyphicon glyphicon-log-in""></span> Login</a>
						</li>
						<li>
							<div id="id02" class="modal">
								<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">×</span>
								<form method="post" class="modal-content animate" action="">
									<div class="container">
										<label><b>Username</b></label>
										<input id="txtUserName" type="text" placeholder="Enter Username" name="txtUserName" required>
										<label><b>Password</b></label>
										<input id="txtPassword" type="password" placeholder="Enter Password" name="txtPassword" required>
										<input type="checkbox" name="cbRemember"> Remember me
										<div class="container" style="background-color:#f1f1f1">
											<button type="submit" class="signupbtn" name="btnLogin">Login</button>
											<button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
											<span class="psw">Forgot <a href="#">password?</a></span>
										</div>
									</div>
								</form>
							</div>
						</li>
						<?php
						} else {
						?>
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="/cart"><i class="fa fa-2x fa-shopping-cart"></i></a>
							</li>
							<li>
								<lavel class="label label-danger" class="badge badge-warning">4</lavel>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i style="font-size: 25px " class="glyphicon glyphicon-user" aria-hidden="true"></i>XinChào <b> <?=$_SESSION["current_user"]->f_Name?></b </a>
								<div class="dropdown-content">
									<a href="profile.php?id=<?=$_SESSION["current_user"]->f_ID?>" class="list-group-item">Thông tin cá nhân</a>
									<a href="http://localhost:8080/laptop/customer/logout" class="list-group-item">Thoát</a>
								</div>
							</li>
						</ul>
						<?php
						}
						?>
					</ul>
				</div>
			</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">
							Giá</h3>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
						</div>
						<div class="panel-body">
							<ul class="list-group">
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox1" type="checkbox">
										<label for="checkbox1">
											0-10tr
										</label>
									</div>
								</li>
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox2" type="checkbox">
										<label for="checkbox2">
											10-20tr
										</label>
									</div>
								</li>
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox3" type="checkbox">
										<label for="checkbox3">
											20tr-30tr
										</label>
									</div>
								</li>
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox4" type="checkbox">
										<label for="checkbox4">
											trên 30tr
										</label>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">
							Nhà sản xuất</h3>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
						</div>
						<div class="panel-body">
							<ul class="list-group">
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox5" type="checkbox">
										<label for="checkbox5">
											Apple
										</label>
									</div>
								</li>
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox6" type="checkbox">
										<label for="checkbox6">
											Asus
										</label>
									</div>
								</li>
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox7" type="checkbox">
										<label for="checkbox7">
											Dell
										</label>
									</div>
								</li>
								<li class="list-group-item">
									<div class="checkbox checkbox-info">
										<input id="checkbox8" type="checkbox">
										<label for="checkbox8">
											Lenovo
										</label>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
					<div class="panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?=$page_title?></h3>
						</div>
						<div class="panel-body">
							<?php include_once $page_body_file;?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="assets/jquery-3.1.1.min.js"></script>
		<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<script type="text/javascript">
					$('#checkbox1').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=10000000');
						}
					});
					$('#checkbox2').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=20000000');
						}
					});
					$('#checkbox3').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=30000000');
						}
					});
					$('#checkbox4').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('index.php?txtSearch=30000069');
						}
					});
					$('#checkbox5').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=1&page=1');
						}
					});					
					$('#checkbox6').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=2&page=1');
						}
					});		
					$('#checkbox7').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=3&page=1');
						}
					});

					$('#checkbox8').click(function() {
						if ($(this).is(':checked')) {
							window.location.replace('productsByCat.php?id=4&page=1');
						}
					});																		
		</script>
	</body>
</html>