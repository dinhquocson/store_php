# ĐỒ ÁN CUỐI KỲ 
# LẬP TRÌNH WEB 1 KHÓA 2015
# NHÓM 12

# Thông tin các thành viên của nhóm:
# 1.Họ tên : Đinh Quốc Sơn
# MSSV : 1560468

# 2.Họ tên : Huỳnh Tấn Tài
# MSSV : 1560480

# 3.Họ tên : Phạm Khánh Tài
# MSSV : 1560485

ĐỀ TÀI : Xây dựng 1 ứng dụng web “Quản lý bán hàng - LAPTOP” gồm các phân hệ cùng các chức năng sau:

1.Phân hệ khách

	1.1 Trang chủ
		+ Hiển thị 10 sản phẩm mới nhất (giảm dần theo ngày tiếp nhận)
		+ Hiển thị 10 sản phẩm bán chạy nhất (giảm dần theo số lượng bán)
		+ Hiển thị 10 sản phẩm được xem nhiều nhất (giảm dần theo số lượt xem)

	1.2 Hệ thống Menu
		+ Hiển thị danh sách loại sản phẩm
		+ Hiển thị danh sách nhà sản xuất

	1.3	Xem danh sách sản phẩm
		+ Xem theo loại
		+ Xem theo nhà sản xuất
		+ Hỗ trợ phân trang

	1.4 Xem chi tiết sản phẩm
		Hiển thị các thông tin sau
				† Hình ảnh
				† Giá bán
				† Số lượt xem
				† Số lượng bán
				† Mô tả
				† Xuất xứ
				† Loại
				† Nhà sản xuất
				† 5 sản phẩm cùng loại
				† 5 sản phẩm cùng nhà sản xuất

	1.5 Tìm kiếm sản phẩm
		+ Cho phép tìm kiếm theo nhiều tiêu chí: tên, giá, loại, nhà sản xuất, 
		+ Hỗ trợ phân trang

	1.6 Đăng nhập

	1.7 Đăng ký
		+ Có sử dụng captcha (khuyến khích dùng Google ReCaptcha)

2.Phân hệ người dùng đã đăng nhập

	2.1 Có đầy đủ chức năng của phân hệ khách

	2.2 Cập nhật thông tin cá nhân 

	2.3 Chọn mua sản phẩm

		•	Cho phép chọn mua sản phẩm khi xem chi tiết hoặc khi xem danh sách sản phẩm

	2.4 Điều chỉnh thông tin sản phẩm đang chọn mua (quản lý giỏ hàng)

	2.5 Thanh toán

		•	Lưu thông tin sản phẩm khách hàng chọn mua và cập nhật lại số lượng bán, số lượng tồn tương ứng cho từng sản phẩm

	2.6 Xem lịch sử mua hàng
		•	Xem danh sách các đơn hàng đã từng đặt theo thứ tự từ mới đến cũ
		•	Xem chi tiết từng đơn hàng và trạng thái của các đơn hàng này

3.Phân hệ quản trị (Admin)

	3.1 Dashboard
		•	Hiển thị danh sách các chức năng mà admin có thể sử dụng

	3.2 Quản lý sản phẩm, loại sản phẩm, nhà sản xuất

	3.3 Quản lý đơn hàng

		•	Thể hiện danh sách đơn hàng theo thứ tự giảm dần của ngày lập.
		•	Cho phép admin thực hiện việc cập nhật trạng thái đơn hàng (chưa giao, đang giao, đã giao). Đơn hàng đã giao sẽ có thể hiện khác với đơn hàng chưa giao (VD: text màu khác, background màu khác, …)
