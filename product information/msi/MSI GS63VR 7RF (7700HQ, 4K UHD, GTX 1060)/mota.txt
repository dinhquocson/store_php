﻿Thật khó để không thích thiết kế mỏng và bề mặt nhôm chải của dòng GS từ một quan điểm thị giác. Thật không may, xây dựng chất lượng đã không được cải thiện đáng kể so với các mô hình GS60 và chúng tôi thấy rằng nó pales so với Razer Blade , Blade Pro , XPS 15, và MacBook Pro series. Các cơ sở là dễ bị xoắn và ọp ẹp và có thể để tràn xuống cả lòng bàn tay và bàn phím bề mặt với một vừa phải đẩy một ngón tay. Nắp có thể uốn cong dễ dàng hơn cả Razer, mặc dù gần như không tốt như trên Aorus X5. Mặc dù chúng tôi không mong đợi khung gầm sẽ có âm thanh như GT72 hoặc GT73VR, nó vẫn có thể chắc chắn hơn và vững chắc hơn khi xem những máy tính xách tay siêu mỏng khác đã được quản lý.

Vật liệu trang điểm mỏng cũng không hoàn hảo trên đơn vị kiểm tra của chúng tôi. Như hai hình ảnh dưới đây, các quạt thông gió gần góc phải của máy tính xách tay không phải là ngồi thẳng với cạnh và thay vào đó nhô ra một chút. Chúng tôi cũng có quan sát tương tự về chiếc GS73VR lớn hơn , vì vậy chúng tôi hy vọng rằng đây không phải là một vấn đề phổ biến rộng rãi với gia đình GS nói chung.

Về kích thước và trọng lượng, hệ thống chỉ dày hơn một milimet hoặc hai lần so với cả XPS 15 và MacBook Pro 15 trong khi hiệu quả hơn cả hai. Nó có một dấu chân lớn hơn cả hai mô hình và cũng nhẹ hơn Dell khoảng 200 g, vì vậy MSI chắc chắn không cảm thấy như dày đặc. Tuy nhiên, hệ thống này đáng ngạc nhiên là xách tay dưới 2 kg so với Alienware 15 nặng hơn và Asus FX502VM .

Các cổng có sẵn rất phong phú và dễ tiếp cận kể từ khi không có ở phía sau của đơn vị. Trên thực tế, mọi lựa chọn từ GS73VR lớn đã chuyển sang GS63VR không bị ảnh hưởng bao gồm cả cổng Thunderbolt 3 quan trọng. Cạnh trái là không may khá đông đúc và các cổng được đặt gần với phía trước, vì vậy cáp dày hơn như HDMI hoặc RJ-45 có khả năng có thể nhận được trong cách. Nó cũng hơi đáng thất vọng rằng không có cổng HDMI 2.0 để dễ dàng tận dụng các màn hình bên ngoài 4K60.

Cập nhật : MSI đã xác nhận với chúng tôi rằng GS63VR 7RF có chuẩn HDMI 2.0 và sẽ xuất ra đến 4K60. Tuy nhiên, việc đặt tên cổng chính thức là HDMI 1.4 do sự chậm trễ trong việc cấp phép.

Đầu đọc thẻ SD
MSI tiếp tục tích hợp một trong những đầu đọc thẻ nhớ chậm nhất trên một máy chơi game cao cấp khác. Nhà sản xuất đã xác nhận với chúng tôi rằng đầu đọc thẻ bị giới hạn bởi bộ điều khiển USB 2.0, do đó, mong đợi tốc độ truyền tải chỉ là 20 MB / s. Chuyển 1 GB ảnh từ thẻ thử nghiệm Toshbia UHS-II lên máy tính để bàn sẽ mất khoảng 43 giây so với 6 đến 7 giây trên Eurocom Tornado F5 và MSI GT73VR .

Bộ đọc thẻ chính nó được nạp vào mùa xuân và một thẻ SD được chèn đầy sẽ nhô ra khoảng một phần ba chiều dài của nó.

Giao tiếp
WLAN và Bluetooth được cung cấp bởi một mô-đun Killer 1535 M2 rời. Chúng tôi không gặp vấn đề về kết nối khi kết nối với bộ định tuyến Linksys EA8500 802.11ac. Killer WLAN cung cấp cả lợi ích phần cứng và phần mềm so với các giải pháp Intel WLAN phổ biến như đã trình bày chi tiết trong bài đánh giá Killer 1535 .

Phụ kiện
Các phụ kiện kèm theo là Hướng dẫn Bắt đầu Nhanh, thẻ bảo hành, khăn lau nhung, và một ổ đĩa DVD / trình điều khiển. Ổ đĩa quang bên ngoài sẽ cần để sử dụng đĩa. Cổng đồ họa GUS và các tiện ích chung Thunderbolt 3 cũng phải tương thích với hệ thống, ít nhất về mặt lý thuyết.

Bảo trì
Bảng dưới cùng chỉ cần một chiếc tuốc nơ vít của Philips để tháo ra, mặc dù người dùng phải cẩn thận với các cạnh và góc sắc nét khi kiểm tra các chốt phía sau. Truy cập vào các thành phần bo mạch chủ chính bao gồm đường ống nhiệt, khe M.2 2280 và khe cắm SODIMM 2x DDR4 yêu cầu phải tháo rời không khác với Ghost GS60 gốc.

Lưu ý rằng việc xóa bảng điều khiển bảo trì phía dưới yêu cầu phải hủy bỏ nhãn dán bảo hành trước tiên. Do đó, người dùng có thể nguy cơ làm mất hiệu lực bảo hành của nhà sản xuất nếu quyết định dịch vụ bất kỳ thành phần nào.

Thiết bị đầu vào

Bàn phím
MSI sử dụng bàn phím quen thuộc của SteelSeries qua các dòng máy chơi game cao cấp và dòng GS cũng không ngoại lệ. Người chơi đã chơi trên GT72 nên cảm thấy ngay tại nhà vì các phím được vững chắc trong phản hồi, không lung lay tại chỗ, và clatter tương đối yên tĩnh. Phản hồi chỉ hơi nhẹ nhàng hơn GT72, tuy nhiên, vì cơ sở bàn phím của GS63VR không phải là cứng nhắc như trên GT series lớn hơn. NumPad nhỏ hơn các phím QWERTY chính và không có phím phụ trợ dành riêng cho các chức năng bổ sung. Ánh sáng RGB ba vùng làm cho sự trở lại của nó, mặc dù một phần nhỏ của chúng tôi hy vọng rằng máy tính xách tay MSI trong tương lai sẽ kết hợp các phím RGB riêng rẽ riêng biệt không giống như các hệ thống Aorus và Razer mới nhất .

Bàn di chuột
Các trackpad nhựa mượt mà hoạt động như người ta mong đợi mà không có sự trắc trở của con trỏ đáng chú ý hoặc các góc không đáng tin cậy. Nhấn vào trung tâm của nó sẽ không đáng kể dọc bề mặt và màng crôm màu đỏ mỏng trông có vẻ lồi hơn lót nhựa in trên nhiều đơn vị ROG của Asus bao gồm G753 . Ít ấn tượng hơn là bàn phím tích hợp của touchpad vì phản hồi mềm và nông. Một lượng công bằng của lực lượng là cần thiết để đăng ký một nhấp chuột, do đó nhấp chuột lặp lại liên tục có thể trở nên mệt mỏi.

Màn hình
Trong khi GS63VR ban đầu của chúng tôi được trang bị bảng điều khiển 1080p, thì đơn vị mới nhất của chúng tôi sử dụng bảng điều khiển UH 4K. Thật không may, màn hình hiển thị là một túi hỗn hợp do một phần của mảng pixel RGBW của nó. Bảng điều khiển không phải là độ phân giải 4K thật sự và các nhà sản xuất khác như Gigabyte và HP gần đây đã bỏ RGBW thay cho thiết lập RGB truyền thống hơn. Trên thực tế, bảng điều khiển Samsung FL156FL02-101 trên GS63VR của chúng tôi là bảng tương tự như trên Asus UX501JW và Eurocom Tornado F5 mới đây , do đó, ba máy tính xách tay này đều có chất lượng hiển thị rất giống nhau.

Mức độ màu đen cao dẫn đến tỉ lệ tương phản giữa khoảng 600: 1 so với 1000: 1 hoặc cao hơn trên nhiều máy tính xách tay chơi game đẳng cấp. Độ sáng của đèn nền là đủ cho ánh sáng xung quanh trong nhà, nhưng chúng tôi hy vọng cho một thứ gì đó sáng hơn so với các hộc 274 đã được ghi lại vì tính di động của hệ thống khuyến khích sử dụng ngoài trời.

Về chủ quan, bảng điều khiển matte là sắc nét với một số hạt nhỏ màu sắc đó là đáng chú ý nhất khi hiển thị một nền trắng về cài đặt độ sáng tối đa. Đơn vị xét nghiệm của chúng tôi cũng cho thấy có một chút ánh sáng đèn nền xung quanh mép dưới không thể thấy được trong quá trình sử dụng hàng ngày.

Hiệu suất

GS63VR có sẵn với CPU i7-7700HQ và một GPU 6 GB GTX 1060 đầy đủ như chúng tôi có. Một SKU riêng với GTX 1060 3 GB cũng là một lựa chọn theo MSI, nhưng chúng ta không thể tìm thấy cấu hình cho thị trường Mỹ vào thời điểm hoặc bằng văn bản. Đáng ngạc nhiên là bộ nhớ RAM không được hàn trên bo mạch và có thể nâng cấp bất kể công trình mỏng và nhẹ.

Optimus là chuẩn cho việc chuyển đổi đồ họa sang GPU HD 630 tích hợp . Do đó, không có tùy chọn G-Sync vì cả hai tính năng này vẫn tiếp tục bị loại trừ lẫn nhau.

Bộ vi xử lý
Hoạt động thô từ i7-7700HQ chỉ tốt hơn một chút so với Skylake i7-6700HQ trước đó trong khối lượng công việc đa luồng. Tăng hiệu suất đơn luồng là cao hơn và ngang bằng với máy tính để bàn i7-6700K. Điều này đặc biệt đáng chú ý vì i7-6700K được đánh giá gần gấp đôi TDP như điện thoại di động i7-7700HQ của chúng tôi.

Xem trang CPU dành riêng của chúng tôi trên i7-7700HQ để biết thêm thông tin kỹ thuật và so sánh điểm chuẩn.

Hiệu suất Hệ thống
PCMark 8 xếp GS63VR của chúng tôi tương tự như các máy tính xách tay chơi game khác trong lớp của nó. Điểm sáng của PCMark Creative, tuy nhiên, đặc biệt tồi tệ hơn FX502VM được trang bị tương tự.

Về chủ quan, chúng tôi không gặp vấn đề về phần mềm hoặc phần cứng trong thời gian sử dụng thường xuyên và tải trò chơi. Thật không may, tải trọng chuẩn với Prime95 và FurMark liên tục sẽ làm hỏng thiết bị kiểm tra của chúng tôi sau nửa phút hoặc lâu hơn. Do đó, chúng tôi không thể cung cấp các phép đo tiếng ồn, nhiệt độ, và tiêu thụ năng lượng thông thường khi phải chịu cả tải Prime95 và FurMark. Nếu không, các trò chơi dường như chạy bình thường và chúng tôi sẽ cung cấp các phép đo của Witcher 3 thay vì thay thế. Chúng tôi khuyên các chủ sở hữu mới của GS63VR sẽ chạy các kiểm tra điểm chuẩn và các công cụ chẩn đoán RAM để đảm bảo rằng hệ thống hoạt động như bình thường khi tải.

Lưu trữ
Cấu hình MSI của chúng tôi được trang bị một ổ SSD Samsung SM961 M.2 NVMe chính và một ổ cứng thứ hai 2Gb Seagate ST2000LM007-1R8174. Tốc độ truyền từ SSD vượt trội so với Apple SM1024L SSD như được tìm thấy trên MacBook Pro 15 mới theo CDM. Tỷ lệ đọc tuần tự rất nhanh với tốc độ trên 2100 MB / s so với khoảng 1600 MB / s trên XPS 15 9560 mới nhất. Trong khi đó, ổ cứng Seagate trả lại tốc độ truyền tải trung bình khoảng 104 MB / s theo HD Tune, trung bình cho một ổ đĩa 7.200 vòng / phút. Ổ cứng 7 mm hoặc SSD được khuyến cáo vì không gian chật hẹp có thể không có ổ 9,5 mm.

Tiếng ồn hệ thống
Làm nhiều người hâm mộ thực sự làm cho một trải nghiệm yên tĩnh hơn? Hệ thống tiếng ồn thực sự là measurably yên tĩnh hơn so với GS60 gửi đi, nhưng không phải bởi càng nhiều càng tốt một trong những hy vọng. Các quạt vẫn luôn hoạt động trên GS63VR ngay cả khi ở chế độ Power Saver với tốc độ quạt được đặt thành Basic. Idling trên máy tính để bàn đã trở nên to hơn hầu hết các máy chơi game hoặc máy tính xách tay đa phương tiện bao gồm Asus FX502VM và XPS 15. Hãy nhớ rằng những người hâm mộ nhạy cảm hơn với những thay đổi RPM khi máy tính xách tay được thiết lập ở Chế độ Balanced hoặc High Performance, được đề nghị khi làm việc trong lớp học hoặc thư viện.

Chạy 3DMark 06 và 3DMark 11 sẽ gây tiếng ồn của quạt nhẹ tương ứng là 37,2 dB (A) và 45,5 dB (A). Để so sánh, FX502VM và XPS 15 9560 sẽ tương ứng là 46,2 dB (A) và 47,1 dB (A) khi chạy 3DMark 11. Trong khi GS63VR hơi ì một chút, nó cũng chịu đựng những người hâm mộ cao hơn chứ không giống như những gì chúng tôi ghi lại trên GS73VR lớn hơn. Ví dụ: các phép đo micro của chúng tôi dưới đây cho thấy một đỉnh điểm hẹp hơn ở dải tần số cao hơn so với đỉnh cao hơn của Asus FX502VM. Điều này có thể là do sự bổ sung của quạt nhỏ hơn ~ 40 mm, do đó, quạt ồn hơn quạt đi kèm với chi phí của một hệ thống hơi cao hơn. Tai nghe được khuyến cáo khi chơi game vì chúng tôi có thể ghi lại tiếng ồn của người hâm mộ là 47 dB (A) khi chơi Witcher 3 .

Lưu ý rằng các phép đo Tải trung bình của chúng tôi dưới đây cho XPS 15 và FX502VM được ghi lại trong khi chạy 3DMark 11 trong khi đo Tải trung bình của chúng tôi cho GS63VR đã được ghi lại trong khi chạy 3DMark 06, vì vậy các giá trị này không thể so sánh trực tiếp với MSI.

Bên ngoài của người hâm mộ, chúng tôi có thể nhận thấy không coil whine trên đơn vị kiểm tra của chúng tôi.