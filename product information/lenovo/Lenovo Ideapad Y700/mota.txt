﻿Ideapad Y700 là mẫu laptop chơi game “entry-level” được thiết kế trên cơ sở nhằm cân bằng giữa tính cơ động và hiệu suất phần cứng. Về ngoại hình, Lenovo Ideapad Y700 trông khá khác biệt với những cỗ máy tính chơi game khác trên thị trường.

Lenovo Ideapad Y700 trông đơn giản và khá “hiền” khi khoác bộ cánh nhôm phay xước màu đen tuyền chủ đạo với vài chi tiết màu đỏ nhấn nhá nhẹ nhàng.

Tuy nhiên, khi mở nắp màn hình, thiết kế góc cạnh của 2 loa bố trí đối xứng qua phần bản lề cùng hệ thống đèn nền LED đỏ rực của Lenovo Ideapad Y700 khiến máy trông cứng cáp hơn hẳn.

Về phương diện phần cứng, Lenovo Ideapad Y700 trang bị màn hình 15,6 inch Full HD, bộ xử lý Intel Core i7-6700HQ xung nhịp chuẩn 2,6GHz, RAM DDR4 dung lượng 8GB, đồ họa rời nVidia GeForce 960M dung lượng RAM đồ họa 4GB, ổ cứng 1TB, pin có thể cho thời lượng sử dụng tầm 4 giờ, công nghệ âm thanh Dolby Home Theater v4, WiFi 802.11ac Wi-Fi, Bluetooth 4.0 và chạy Windows 10.

Từ sử dụng thực tế, Test Lab nhận thấy Lenovo Ideapad Y700 có thiết kế cứng cáp, góc xoay bản lề màn hình rộng, thoải mái cho việc tùy chỉnh góc nhìn của người dùng.

Bên cạnh thiết kế đôi loa JBL hướng thẳng về phía game thủ, Lenovo Ideapad Y700 còn được ưu ái trang bị kèm loa sub ở mặt dưới. Máy cũng trang bị khá nhiều giao tiếp cần thiết như USB 3.0, Ethernet, HDMI, đầu đọc thẻ và cả ổ đĩa quang gắn ngoài bán kèm. Chỉ tiếc là Lenovo Ideapad Y700 không trang bị ngõ USB Type-C và phần vỏ máy rất nhạy dấu vân tay trong quá trình sử dụng.

Về bàn phím, Test Lab đánh giá cao thiết kế hỗ trợ tùy chỉnh 2 mức độ sáng của đèn nền LED trên Lenovo Ideapad Y700. Touchpad lớn, các nút chức năng chuột trái – phải hoạt động êm ái cũng là một điểm đáng chú ý của chiếc laptop chơi game này. Xuyên suốt quá trình trải nghiệm thực tế, Test Lab cảm thấy hài lòng với thiết kế hỗ trợ hầu hết cử chỉ điều khiển đa điểm của nền tảng Windows trên touchpad này.

Tuy vậy, thiết kế bề mặt thiếu đi sự mượt mà của touchpad có thể sẽ khiến người dùng khó chịu khi điều hướng trong những thao tác hằng ngày. Cũng từ thực tế sử dụng cho thấy bàn phím trên Lenovo Ideapad Y700 tuy có hành trình thoải mái, cụm phím WASD cũng được thiết kế nổi bật và còn trang bị cả cụm phím số, song cảm giác phím khi làm việc với văn bản cũng như khi chơi game vẫn chưa thật tốt như mong đợi và nhà sản xuất dường như cũng quên không trang bị cho bàn phím của máy các phím macro.

Chất lượng hiển thị của màn hình 15,6 inch trên Lenovo Ideapad Y700 tuy không ấn tượng như những mẫu laptop chơi game đắt đỏ khác, nhưng những kết quả thử nghiệm cho thấy khá ổn so với tầm giá và mác “entry-level”. Nói một cách cụ thể hơn, Lenovo Ideapad Y700 thành công trong việc tái hiện sắc đen nhờ độ tương phản tốt.

Chất lượng màu sắc tổng thể dẫu có phần ngả vàng, song vẫn tái hiện khá thành công màu xanh của bầu trời trong Fallout 4 cũng như Rainbow Six: Siege. Nói một cách ngắn gọn là chất lượng màu sắc, độ sắc nét của Lenovo Ideapad Y700 vừa vặn cho nhu cầu giải trí với game cũng như phim ảnh.

Không chỉ cho chất lượng hình ảnh tốt so với tầm giá, Lenovo Ideapad Y700 còn gây ấn tượng tốt bởi chất lượng âm thanh từ đôi loa JBL cộng thêm loa sub nằm ở mặt dưới máy.

Thực tế cho thấy hệ thống 3 loa trên Lenovo Ideapad Y700 đủ nhấn chìm game thủ khi giải trí trong một không gian nhỏ. Hệ thống loa này xứng đáng mang về cho Lenovo Ideapad Y700 điểm cộng đáng giá bởi khả năng cho âm lượng lớn, chất âm khá đồng đều, chi tiết và còn có độ vang. Thử nghiệm chất âm trên 2 mẫu tai nghe HyperX Cloud và cả Sennheiser HD 419 đều cho thấy khả năng tái hiện không gian âm thanh trong game của Lenovo Ideapad Y700 tốt như mong đợi vì Test Lab dường như đã trở thành nhân vật chính trong game từ lúc nào.

Về hiệu năng, với sức mạnh phần cứng như đã đề cập, Lenovo Ideapad Y700 lẽ đương nhiên có thừa sức mạnh cho nhu cầu giải trí đa phương tiện, cũng như đa nhiệm hằng ngày. Thực tế đo kiểm cũng đã minh chứng cho điều này khi hầu hết kết quả thử nghiệm hiệu năng bộ xử lý, hiệu năng đồ họa đều tốt như mong đợi. So với mẫu Lenovo Y50 từng thử nghiệm, Lenovo Ideapad Y700 trong hầu hết phép thử đều cho thấy những cải tiến vượt trội nhờ sức mạnh phần cứng được đầu tư đáng kể.

Trong các phép thử với vài tựa game đình đám của 2014 cũng như 2015, Lenovo Ideapad Y700 đều cho thấy kết quả khung hình vượt trên mức 30 khung hình/giây cả khi cài đặt độ phân giải game ở mức Full HD và độ chi tiết đồ họa cao.

Với sức mạnh từ card đồ họa nVidia GeForce 960M vốn có hiệu suất cao hơn GTX 750 Ti, đi kèm bộ xử lý Intel Skylake và 8GB RAM, Test Lab khẳng định Lenovo Ideapad Y700 đủ sức cân tốt những tựa game lớn được ra mắt trong năm 2015 hoặc trở về trước mà người dùng không phải hy sinh nhiều hiệu ứng đồ họa, vật lý của game.

Tuy vậy, với những trường hợp đặc biệt như Assassin's Creed Unity quá trình hiện thực hóa số liệu cho thấy Test Lab buộc phải chọn lựa giữa độ phân giải hoặc độ chi tiết, hiệu ứng đồ họa để các cảnh hành động trong game mượt mà hơn. Điều này thực ra cũng dễ hiểu và không nằm ngoài dự đoán của Test Lab vì cơ bản, nVidia GeForce 960M cũng chỉ được xếp nhỉnh hơn mức tầm trung và lẽ đương nhiên không thể so sánh với những đàn anh cao cấp khác ra mắt sau đó.

 Về nhiệt độ, Test Lab đánh giá cao khả năng làm mát của Lenovo Ideapad Y700 vì khu vực nửa bên trái bàn phím - vốn là nơi cụm phím WASD tọa lạc không hề bị nóng khi trải nghiệm với game hàng giờ liên tục trong môi trường thử nghiệm có nhiệt độ tầm 28 độ C. Máy cũng vận hành khá êm ái dù khi bị ép tải với Assassin's Creed Unity, Fallout 4 thiết lập đồ họa ở mức High cũng như khi thử nghiệm vắt kiệt sức bộ xử lý bằng những công cụ chuyên dụng. Xuyên suốt quá trình trải nghiệm, khu vực nửa bên phải bàn phím và cụm phím số khá nóng, song vẫn ở mức dễ chịu.

Nhìn chung, Test Lab đánh giá cao hiệu năng game của Lenovo Ideapad Y700 cũng như khả năng xử lý đa nhiệm trong giải trí đa phương tiện hằng ngày. Máy cũng có thiết kế cứng cáp, chất lượng âm thanh loa tích hợp tốt như mong đợi và giá bán cũng khá dễ chịu. Tiếc là thiết kế mặt lưng máy có thể gây ít nhiều khó khăn cho người dùng khi cần nâng cấp, bảo trì, vỏ máy nhạy dấu vân tay và thiết kế tổng thể khá nặng.
