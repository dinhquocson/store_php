﻿Trước tiên nói về khối lượng thì MacBook Pro 13 (2016) chỉ nặng 1,37 kg, trong khi MacBook Air 13 là 1,35 kg. Sự chênh lệch rất ít về khối lượng sẽ không ảnh hưởng nhiều đến việc bạn mang máy di chuyển hàng ngày.
​
Tuy nhiên yếu tố quan trọng hơn của hai mẫu máy tính 13-inch này chính là khi so sánh kích thước. Khi ra mắt MacBook Pro 13 Retina vào năm 2012, Apple đã thu gọn kích thước chiều dài và chiều rộng của nó là 31,4 x 21,9 cm; tức là gọn hơn MacBook Air 13 (32.5 x 22.7 cm). Nếu bạn cho rằng vài milimet đó không ảnh hưởng gì thì mình chia sẻ một câu chuyện thú vị thế này: bạn mình mua một túi đựng (laptop sleeve) dành cho MacBook Pro 13 và hy vọng nó dùng được cho… MacBook Air 13. Thật trớ trêu khi mua về nó không thể khớp vừa cái chốt cài của túi vì chiều rộng của MacBook Air 13 lớn hơn.

Thử nghiệm thực tế chiếc túi Think Tank Perception tablet được tạo ra để đựng máy tính bảng tầm 10-12”. Mình đã sử dụng nó được hơn 1 năm để đựng MacBook Pro 13 Retina rất tốt, tuy nhiên nếu sử dụng để đựng MacBook Air 13 lại không vừa

Thực tế rõ ràng là bạn có thể mua những chiếc túi xách hay balo được ghi là dùng cho máy tính 11” hoặc 12” thì MacBook Pro 13 Retina trước đây đều có thể bỏ vừa. Vì vậy không lý do gì mà MacBook Pro 13 (2016) lại không thể là một lựa chọn tốt hơn cho nhu cầu di động.

Điều quan trọng hơn là việc MacBook Pro 13 (2016) sở hữu màn hình retina tương tự như MacBook Pro 13 Retina (2012-2015) cũng khiến cho việc mình không phải suy nghĩ đến MacBook Air 13 nữa (tất nhiên phải vượt qua rào cản là mức giá 1.300 USD).

Trong bảng thông số trên trang web Apple, bạn có thể thấy MacBook Pro 13 (2016) sử dụng vi xử lý Intel Core i5 lõi kép 2.0 GHz, turbo boost 3.1 GHz cùng bộ nhớ đệm L3 dung lượng 4MB; tuỳ chọn cao hơn là Core i7 lõi kép 2.4 GHz, turbo boost 3.4 GHz. Tất cả đều sử dụng chip đồ hoạ Intel Iris Graphics 540.

Qua kiểm tra cấu hình trên chiếc MacBook Pro 13 (2016) cấu hình cơ bản vừa đập hộp, CPU mang tên mã là i5-6360U cho mức tiêu thụ điện năng tối đa (max TDP) là 15W. Tức là mức TDP tương đương với CPU i5-5250U trên MacBook Air 13 (late 2015). Trong khi đó cả MacBook Pro 13 Retina (2015) và MacBook Pro 13 (2016) với touch bar đều sử dụng CPU có TDP ở mức 28W. Chắc chắn rằng những thứ 'hiện đại’ sẽ hại điện hơn.

Trở lại với MacBook Air 13, chiếc máy này pin dung lượng là 54 Wh, trong khi đó MacBook Pro 13 (2016) có dung lượng cao hơn không đáng kể là 54.5 Wh. Với màn hình retina thì MacBook Pro 13 (2016) có thể sẽ tiêu hao năng lượng nhiều hơn so với MacBook Air. Bù lại thời lượng pin nếu so với MacBook Pro 13 (2016) với touch bar sẽ cao hơn do cấu hình của phiên bản với CPU có TDP cao hơn (28W), trong khi pin dung lượng 49.2 Wh thấp hơn bởi thiết kế phiên bản touch bar có hai quạt tản nhiệt.

Khi giới thiệu, nhiều người nhìn vào thấy rằng MacBook Pro 13 (2016) có giá khởi điểm cao hơn so với thế hệ trước. Tuy nhiên nếu so sánh với mức giá MacBook Pro 13 Retia (2015) MF840 có SSD 256GB thì lại bằng nhau. Kết quả benchmark giữa hai phiên bản MacBook Pro 13 (2016) và MacBook Pro 13 Retina (2015) MF840 cho thấy sự chênh lệch hiệu năng không quá lớn, trong khi SSD có tốc độ gấp đôi và đồ hoạ cũng được cải thiện đáng kể.

