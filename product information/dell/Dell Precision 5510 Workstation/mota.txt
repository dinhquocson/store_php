﻿Dell Precision M5510 đã viết lại định nghĩa về dòng Mobile Workstation vốn cồng kềnh và thô cứng. Mang đến một hình ảnh mới và quyến rũ cho người dùng.

Bao giờ thiết kế một cỗ máy cũng phải hài hòa với hiệu năng của chiếc máy đó. Những chiếc máy có hiệu năng cao, mạnh mẽ luôn đi kèm với kích thước lớn, điện năng tiêu thụ nhiều đó là điều không thể tránh khỏi. Năm 2016, Dell cho ra mắt dòng máy trạm với bước tiến hoàn toàn mới. Đây có thể nói là thay đổi lớn nhất từ trước của Dell trong phân khúc máy trạm di động.

Dell đã tiếp nối sự thành công của thiết kế màn hình không viền - infinity trên XPS 13 và XPS 15 bằng Mobile Workstation Dell Precision 5510. Với tùy chọn độ phân giải màn hình lên tới 4K (3860x2160), cùng với bộ vi xử lý Xeon, card đồ họa NVIDIA Quadro chuyên dụng đồ họa. Precision 5510 được bán với giá từ 1300USD đến 2600USD tùy vào cấu hình tại Mỹ. Đây thực sự là một mẫu máy trạm di động được đánh giá là đẹp nhất hiện nay.

Giống với Dell XPS 15, Precision 5510 có thiết kế siêu mỏng, mặt trên và được làm bằng nhôm đen - bạc. Bên trong phần kê tay được làm bằng chất liệu sợi carbon. Tất cả những vật liệu đó phủ lên làm cho Precision 5510 thể hiện được sự cao cấp và cảm giác sử dụng rất tốt.

 Để hi sinh cho vẻ ngoài sexy, thì phần bàn phím của Precision 5510 khi gõ sẽ có cảm giác hơi cạn. Hành trình phím ngắn hơn một chút. Tuy nhiên, với sự hoàn thiện cao cấp, các phím bấm đủ chắc chắn và layout phím hợp lý. Thì việc làm quen với bàn phím Precision 5510 là rất nhanh. 

Touchpad trên Dell Precision 5510 rộng rãi và thoải mái. Cảm giác di rất tốt trên Windows 10. Đây là bàn di touchpad đa điểm.

Màn hình của Dell Precision 5510 cho chất lượng hiển thị cực tốt, độ sáng cao - lên tới 322nits (cao hơn so với các đối thủ như ThinkPad W550s 312nits và MSI WS60 216nits). Góc nhìn gần như tối đa 180 độ.

Khả năng thể hiện màu sắc sRGB trên Precision 5510 cũng tốt hơn hẳn so với W550s hay WS60. Nó thể hiện được 177% gamut, trong khi của W550s và WS60 là khoảng 80%.

Về độ chính xác màu Delta-E của Precision 5510 là 2,6. Tốt hơn ThinkPad W550s là 2,72 và hơn nhiều so với MSI WS60 là 11,6

Các cổng kết nối trên Dell Precision 5510 gồm có: SD Card Reader (SD, SDHC, SDXC, supporting up to 64GB), Thunderbolt 3, 2 cổng USB 3.0, giắc cắm tai nghe đi kèm mic và cổng HDMI

Với bộ vi xử lý Intel Xeon E3 1505M v5, RAM 16GB, VGA Nvidia Quadro M1000M 2GB đi kèm theo đó là ổ cứng với công nghệ PCIe tốc độ siêu cao, thì chiếc Dell Precision 5510 có thể đáp ứng tốt các tác vụ xử lý đồ họa cao cấp. Hiệu năng tổng thể chấm theo Geekbench 3 thì Precision 5510 đạt trên 14000 điểm, tốt hơn rất nhiều các đối thủ cùng phân khúc như W550s, WS60, Dell XPS 15...

Do hiệu năng vượt trội hơn ThinkPad W550s nên việc tiêu tốn pin hơn là không tránh khỏi. Thời lượng sử dụng Pin trên Dell Precision 5510 đạt mức trung bình - khoảng 5h34p sử dụng thực tế ở mức độ bình thường. Đây là thời lượng Pin chấp nhận được với một chiếc máy trạm di động.

