﻿máy trạm Dell Precision 3510 tương đương với máy trạm Latitude 5000 hiện tại và do đó cung cấp các tính năng kinh doanh xuất sắc. Màu đen và màu xám tạo ra thiết kế hạn chế mà nhiều khách hàng doanh nghiệp muốn, vì vậy bạn có thể tập trung hoàn toàn vào dự án tiếp theo.

Một điểm nổi bật trong phạm vi giá này chắc chắn là cổng Thunderbolt 3 đầy đủ. Tuy nhiên, hiện tại nó không có sẵn kết hợp với i5 bộ vi xử lý mà là một giới hạn không cần thiết cho một số người dùng. Bạn có thể đính kèm các ổ Thunderbolt 3 tốc độ lên đến 40 GB / s tại cổng này, cũng như ổ USB 3.1 với tốc độ lên đến 10 GB / s. Các màn hình ngoài 4K có thể được điều khiển ở tần số 60 Hz.

Dock Thunderbolt mới của Dell cũng hoạt động tốt với thiết bị, và các bến cảng này có lẽ sẽ thay thế các vị trí độc quyền truyền thống (vẫn có thể được gắn ở cuối) trong tương lai. Nếu không, bạn vẫn có được một cổng VGA và HDMI, một thiết bị rất phổ biến cho các thiết bị tiêu dùng.

Lựa chọn cổng toàn diện và vị trí được bổ sung bởi ba cổng USB 3.0, một đầu đọc thẻ UHS-II và khe cắm âm thanh stereo 3.5 mm kết hợp.

Bàn phím của Dell Precision 3510 rất thích hợp cho các nhà văn giàu có và có các phím lớn (19 mm / ~ 0,75 inch) cũng như bàn phím số riêng biệt. Bàn phím matte rất chắc, không bị trả lại và cung cấp phản hồi tốt với một điểm áp lực được xác định rõ ràng. Các chữ cái dễ nhìn thấy và được hỗ trợ thuận tiện bởi sự chiếu sáng bàn phím 2 giai đoạn.

Touchpad cung cấp khả năng trượt rất tốt và nhận ra đầu vào khá đáng tin cậy. Các nút touchpad chuyên dụng được trơn tru và yên tĩnh khi bạn nhấn chúng. Các cử chỉ đa cảm thông thường được hỗ trợ và bạn có thể mở khóa thêm hoặc ngừng các tính năng không cần thiết trong trình điều khiển trên touchpad.

Các thiết bị dành cho doanh nghiệp thường được trang bị TrackPoint như một sự thay thế chuột bổ sung. Đây là trường hợp ở đây và cũng có các nút riêng biệt trên bàn di chuột. Tuy nhiên, sự thoải mái của giải pháp không hoàn toàn nằm ở mức ThinkPad vì bề mặt lõm khó nhả ra từ các phím xung quanh và do đó rất khó sử dụng.

Intel Core i7-6700HQ là một bộ xử lý 4 nhân mạnh mẽ, hỗ trợ Hyperthreading cũng như Turbo Boost và do đó có thể cung cấp hiệu suất tốt nhất cho các kịch bản sử dụng khác nhau. Các ứng dụng đơn sợi đơn được hưởng lợi từ các đồng hồ cao lên đến 3,5 GHz, và phần mềm đa luồng có thể sử dụng tối đa tám lõi đồng thời.

Các điểm chuẩn của chúng tôi đặt bộ xử lý nhẹ đằng sau  CPU Intel Xeon E3-1505 , cũng có sẵn cho Precision 3510. Hiệu suất khác biệt giữa 20 và 9% vẫn còn ở mức vừa phải. Cũng có những lý do khác cho bộ vi xử lý Xeon như hiệu suất được cải thiện dưới tải bền vững và hỗ trợ ECC-RAM. Tính năng này hứa hẹn sự ổn định và độ chính xác cao hơn trong các nhiệm vụ đòi hỏi và bộ nhớ nhờ vào việc sửa lỗi được cải thiện. Cho dù hiệu suất hoạt động nhỏ của CPU Xeon có thể được sử dụng với Dell Precision 3510, tuy nhiên, không rõ ràng vào thời điểm này.

Thử nghiệm căng thẳng của chúng tôi cho thấy đồng hồ bộ xử lý của  Intel Core i7-6700HQ  giảm xuống 2300 MHz sau một giờ thử nghiệm căng thẳng của chúng tôi, và do đó dưới mức đồng hồ cơ sở là 2600 MHz. HWiNFO cho thấy nhiệt độ lõi lên tới 99 ° C (~ 210 ° F), vì vậy không chắc liệu Precision có cung cấp quạt làm mát cho một CPU mạnh hơn hay không. Hiệu suất của  AMD FirePro W5130M  cũng giảm trong kịch bản này, bởi vì theo GPU-Z, chip chạy chỉ với 475 MHz.

Các phép đo điện năng của chúng tôi cho mô hình thử nghiệm của Dell Precision 3510 là từ 4,2 đến 92,2 watt. Tiêu thụ tối thiểu, thậm chí giảm xuống chỉ còn 2,5 watt khi chúng tôi hủy kích hoạt màn hình. Thiết bị này vẫn khá tiết kiệm trong các tình huống thực tế hơn (không dây, độ sáng 150 nits, cân bằng công suất) ở mức 8 watt. Những con số này cho thấy thời gian chạy pin tốt kết hợp với pin 84-Wh lớn. Dell cũng cung cấp một phiên bản 62-Wh nhỏ hơn, cần thiết để thực hiện một ổ đĩa 2,5-inch thay vì M.2-SSD.

Thời gian chạy pin đã xác định gần như là hiện tượng đối với máy trạm di động. Bên cạnh các thành phần cân bằng và linh hoạt, cũng được điều chỉnh nhẹ dưới tải, pin lớn đặc biệt có tác động tích cực đến thời gian chạy.

Chúng tôi có thể đo thời gian chạy giữa 19 giờ (chế độ đọc ở độ sáng tối thiểu và không dây được tắt) và gần 2 giờ (Battery Eater Classic Test, độ sáng tối đa, không dây, hiệu năng cao). Các thiết lập thông thường (150 nits, cân bằng hồ sơ) sẽ cho kết quả thời gian thực tế của khoảng 9 đến 10 giờ.

